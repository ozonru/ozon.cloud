﻿import * as React from 'react'

export class Login extends React.Component<any, void> {
    public render() {
        return <div>
            <h2>Hello, %username%</h2>
            <form method="POST" action="">
                <div className="form-group">
                    <label>Email address</label>
                    <input type="email" className="form-control" id="email-input" aria-describedby="emailHelp" placeholder="Enter email" />
                    <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
                <div className="form-group">
                    <label>Password</label>
                    <input type="password" className="form-control" id="password-input" aria-describedby="emailHelp" placeholder="Enter password" />
                </div>
                <button type="submit" className="btn btn-primary">Submit</button>
            </form>
        </div>;
    }
}
