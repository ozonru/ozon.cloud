import * as React from 'react';
import { Router, Route, HistoryBase } from 'react-router';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { FetchData } from './components/FetchData';
import { Counter } from './components/Counter';
import { Users } from './components/Users';
import { Login } from './components/auth/Login';
import { Restrictions } from './components/checkout/Restrictions';

export default <Route component={ Layout }>
    <Route path='/' components={{ body: Home }} />
    <Route path='/counter' components={{ body: Counter }} />
    <Route path='/fetchdata' components={{ body: FetchData }} />
    <Route path='/users' components={{ body: Users }} />
    <Route path='/account/login' components={{ body: Login }} />
    <Route path='/checkout/deliveryOptions' components={{ body: Restrictions }} />


</Route>;

// Allow Hot Module Reloading
declare var module: any;
if (module.hot) {
    module.hot.accept();
}
